/*
// Simple DIY Electronic Music Projects
//    diyelectromusic.wordpress.com
//
//  Arduino MIDI VS10xx Synth
//  https://diyelectromusic.wordpress.com/2021/01/09/arduino-midi-vs1003-synth/
//
      MIT License
      
      Copyright (c) 2020 diyelectromusic (Kevin)
      
      Permission is hereby granted, free of charge, to any person obtaining a copy of
      this software and associated documentation files (the "Software"), to deal in
      the Software without restriction, including without limitation the rights to
      use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
      the Software, and to permit persons to whom the Software is furnished to do so,
      subject to the following conditions:
      
      The above copyright notice and this permission notice shall be included in all
      copies or substantial portions of the Software.
      
      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
      FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHERIN
      AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
      WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
/*
  Using principles from the following Arduino tutorials:
    Arduino MIDI Library    - https://github.com/FortySevenEffects/arduino_midi_library
    VS10xx software patches - http://www.vlsi.fi/en/support/software/vs10xxpatches.html
    VS10xx applications     - http://www.vlsi.fi/en/support/software/vs10xxapplications.html

  This uses a generic VS1003 or VS1053 MP3 Player board utilising
  VS10xx software patching to provide a robust "real time MIDI" mode
  with MIDI access provided over the SPI bus.

  If you want an easier time of things, be sure to get a recognised
  board that supports the VS1053 more completely such as those from
  Adafruit or Sparkfun.

  But this code is pretty universal on non-name VS1003 and VS1053
  based modules. BENZ
*/
#include <SPI.h>
#include <MIDI.h>
#include "vs10xx_uc.h" // From VLSI website: http://www.vlsi.fi/en/support/software/microcontrollersoftware.html
#include <ezButton.h>

ezButton limitSwitch(2);  // create ezButton object that attach to pin 7;

#define TEST 1

// Uncomment to perform a test play of the instruments and drums on power up
#define SOUND_CHECK 1

// Uncomment this if you have a test LED
// Note: The LED_BUILTIN for the Uno is on pin 13,
//       which is used below for the VS1003 link.
#define MIDI_LED 4

// This code supports several variants of the VS10xx based shields.
// Choose the apprppriate one here (and comment out the others).
//
// #define VS1053_MP3_SHIELD 1
#define VS1003_MODULE 1

#ifdef VS1003_MODULE
extern "C" {
#include "rtmidi1003b.h"
#include "vs1003inst.h"
}
#endif
#ifdef VS1053_MP3_SHIELD
extern "C" {
#include "rtmidi1053b.h"
}
#endif

// Use binary to say which MIDI channels this should respond to.
// Every "1" here enables that channel. Set all bits for all channels.
// Make sure the bit for channel 10 is set if you want drums.
//
//                               16  12  8   4  1
//                               |   |   |   |  |
uint16_t MIDI_CHANNEL_FILTER = 0b1111111111111111;

// Comment out any of these if not in use
//#define POT_MIDI  A0 // MIDI control
//#define POT_VOL   A1 // Volume control

#ifdef VS1053_MP3_SHIELD
// VS1053 Shield pin definitions
#define VS_XCS    6 // Control Chip Select Pin (for accessing SPI Control/Status registers)
#define VS_XDCS   7 // Data Chip Select / BSYNC Pin
#define VS_DREQ   2 // Data Request Pin: Player asks for more data
#define VS_RESET  8 // Reset is active low
#endif
#ifdef VS1003_MODULE
// VS1003 Module pin definitions
#define VS_XCS    8 // Control Chip Select Pin (for accessing SPI Control/Status registers)
#define VS_XDCS   9 // Data Chip Select / BSYNC Pin
#define VS_DREQ   7 // Data Request Pin: Player asks for more data
#define VS_RESET  10 // Reset is active low
#endif
// VS10xx SPI pin connections (both boards)
// Provided here for info only - not used in the sketch as the SPI library handles this
#define VS_MOSI   11
#define VS_MISO   12
#define VS_SCK    13
#define VS_SS     10

// Optional - use Digital IO as the power pins
//#define VS_VCC    6
//#define VS_GND    5

// There are three selectable sound banks on the VS1053
// These can be selected using the MIDI command 0xBn 0x00 bank
#define DEFAULT_SOUND_BANK 0x00  // General MIDI 1 sound bank
#define DRUM_SOUND_BANK    0x78  // Drums
#define ISNTR_SOUND_BANK   0x79  // General MIDI 2 sound bank

// List of instruments to send to any configured MIDI channels.
// Can use any GM MIDI voice numbers here (1 to 128), or more specific definitions
// (for example as found in vs1003inst.h for the VS1003).
//
// 0 means "ignore"
//
byte preset_instruments[16] = {
/* 01 */  1,
/* 02 */  9,
/* 03 */  17,
/* 04 */  25,
/* 05 */  30,
/* 06 */  33,
/* 07 */  41,
/* 08 */  49,
/* 09 */  57,
/* 10 */  0,  // Channel 10 will be ignored later as that is percussion anyway.
/* 11 */  65,
/* 12 */  73,
/* 13 */  81,
/* 14 */  89,
/* 15 */  113,
/* 16 */  48
};

// This is required to set up the MIDI library.
// The default MIDI setup uses the built-in serial port.
MIDI_CREATE_DEFAULT_INSTANCE();

byte instrument;
byte volume;

#ifdef TEST
char teststr[32];
#endif

void setup() {
limitSwitch.setDebounceTime(50); // set debounce time to 50 milliseconds
#ifdef TEST
  Serial.begin(9600);
  Serial.println ("Initialising VS10xx");
#endif // TEST

#ifdef VS_VCC
  pinMode(VS_VCC, OUTPUT);
  digitalWrite(VS_VCC, HIGH);
#endif // VS_VCC
#ifdef VS_GND
  pinMode(VS_GND, OUTPUT);
  digitalWrite(VS_GND, LOW);
#endif // VS_GND
#ifdef MIDI_LED
  pinMode(MIDI_LED, OUTPUT);
  // flash the LED on startup
  for (int i=0; i<4; i++) {
    digitalWrite(MIDI_LED, HIGH);
    delay(100);
    digitalWrite(MIDI_LED, LOW);
    delay(100);
  }
#endif // MIDI_LED

  // put your setup code here, to run once:
  initialiseVS10xx();

  // This listens to all MIDI channels
  // They will be filtered out later...
#ifndef TEST
  MIDI.begin(MIDI_CHANNEL_OMNI);
#endif // TEST

  delay(1000);

#ifdef SOUND_CHECK
  // A test "output" to see if the VS0xx is working ok
#ifdef POT_MIDI
  byte ch = POT_MIDI_CHANNEL-1;
#else  // POT_MIDI
  byte ch = 0;
#endif // POT_MIDI
#endif // SOUND_CHECK
  instrument = -1;
  volume = -1;

  talkMIDI (0x99, 38, 127);
}

void loop() {

//# Piezo module definitions
int a0_bass = analogRead(A0);
int a1_snare = analogRead(A1);
int a2_tom1 = analogRead(A2);
int a3_tom2 = analogRead(A3);
int a4_hihat = analogRead(A4);
int a5_char = analogRead(A5);

limitSwitch.loop(); // MUST call the loop() function first
int state = limitSwitch.getState();
// if(limitSwitch.isPressed())
//   Serial.println("The limit switch: UNTOUCHED -> TOUCHED");

// if(limitSwitch.isReleased())
//   Serial.println("The limit switch: TOUCHED -> UNTOUCHED");




if (a2_tom1 >= 400) {
  Serial.print("TOM1:");
  Serial.println(a2_tom1);
  // digitalWrite(13, 1);
  talkMIDI (0x99, 50, 127);
} else {
  // Serial.println(a2_tom1);
  talkMIDI (0x99, 50, 0);
  // digitalWrite(13, 0);
}

if (a3_tom2 >= 400) {
  Serial.print("TOM2:");
  Serial.println(a3_tom2);
  // digitalWrite(13, 1);
  talkMIDI (0x99, 43, 127);
} else {
  // Serial.println(a3_tom2);
  talkMIDI (0x99, 43, 0);
  // digitalWrite(13, 0);
}

// Serial.println(state);
if(state == HIGH) {
   if (a4_hihat >= 300) {
    Serial.println("The limit switch: UNTOUCHED");
    Serial.print("HIHAT_O:");
    Serial.println(a4_hihat);
    // digitalWrite(13, 1);
    talkMIDI (0x99, 46, 120);
    // delay(5);
  } else {
    talkMIDI (0x99, 46, 0);
  // digitalWrite(13, 0);
  }
}
else {
  if (a4_hihat >= 300) {
    Serial.println("The limit switch: TOUCHED");
    Serial.print("HIHAT_C:");
    Serial.println(a4_hihat);
    // digitalWrite(13, 1);
    talkMIDI (0x99, 42, 120);
    // delay(5);
  } else {
    talkMIDI (0x99, 42, 0);
  // digitalWrite(13, 0);
  }
}



if (a5_char >= 1023) {
  Serial.print("CHAR:");
  Serial.println(a5_char);
  // digitalWrite(13, 1);
  talkMIDI (0x99, 57, 110);
} else {
  talkMIDI (0x99, 57, 0);
  // digitalWrite(13, 0);
}


if (a0_bass >= 100) {
  Serial.print("BASS:");
  Serial.println(a0_bass);
  // digitalWrite(13, 1);
  talkMIDI (0x99, 35, 100);
  // talkMIDI (0x99, 35, 0);
  // delay(1000);
} else {
  // Serial.print("BASS:");
  // Serial.println(a0_bass);
  talkMIDI (0x99, 35, 0);
  // digitalWrite(13, 0);
}

if (a1_snare >= 300) {
  Serial.print("SNARE:");
  Serial.println(a1_snare);
  // digitalWrite(13, 1);
  talkMIDI (0x99, 38, 100);
} else {
  talkMIDI (0x99, 38, 0);
  // digitalWrite(13, 0);
}
}
/***********************************************************************************************
 * 
 * Here is the code to send MIDI data to the VS1053 over the SPI bus.
 *
 * Taken from MP3_Shield_RealtimeMIDI.ino by Matthias Neeracher
 * which was based on Nathan Seidle's Sparkfun Electronics example code for the Sparkfun 
 * MP3 Player and Music Instrument shields and and VS1053 breakout board.
 *
 ***********************************************************************************************
 */
void sendMIDI(byte data) {
  SPI.transfer(0);
  SPI.transfer(data);
}

void talkMIDI(byte cmd, byte data1, byte data2) {
  //
  // Wait for chip to be ready (Unlikely to be an issue with real time MIDI)
  //
  while (!digitalRead(VS_DREQ)) {
  }
  digitalWrite(VS_XDCS, LOW);

  sendMIDI(cmd);
  
  //Some commands only have one data byte. All cmds less than 0xBn have 2 data bytes 
  //(sort of: http://253.ccarh.org/handout/midiprotocol/)
  // SysEx messages (0xF0 onwards) are variable length but not supported at present!
  if( (cmd & 0xF0) <= 0xB0 || (cmd & 0xF0) >= 0xE0) {
    sendMIDI(data1);
    sendMIDI(data2);
  } else {
    sendMIDI(data1);
  }

  digitalWrite(VS_XDCS, HIGH);
}


/***********************************************************************************************
 * 
 * Code from here on is the magic required to initialise the VS10xx and 
 * put it into real-time MIDI mode using an SPI-delivered patch.
 * 
 * Here be dragons...
 * 
 * Based on VS1003b/VS1033c/VS1053b Real-Time MIDI Input Application
 * http://www.vlsi.fi/en/support/software/vs10xxapplications.html
 *
 * With some input from MP3_Shield_RealtimeMIDI.ino by Matthias Neeracher
 * which was based on Nathan Seidle's Sparkfun Electronics example code for the Sparkfun 
 * MP3 Player and Music Instrument shields and and VS1053 breakout board.
 * 
 ***********************************************************************************************
 */

void initialiseVS10xx () {
  // Set up the pins controller the SPI link to the VS1053
  pinMode(VS_DREQ, INPUT);
  pinMode(VS_XCS, OUTPUT);
  pinMode(VS_XDCS, OUTPUT);
  pinMode(VS_RESET, OUTPUT);

  // Setup SPI
  // The Arduino's Slave Select pin is only required if the
  // Arduino is acting as an SPI slave device.
  // However, the internal circuitry for the ATmeta328 says
  // that if the SS pin is low, the MOSI/MISO lines are disabled.
  // This means that when acting as an SPI master (as in this case)
  // the SS pin must be set to an OUTPUT to prevent the SPI lines
  // being automatically disabled in hardware.
  // We can still use it as an OUTPUT IO pin however as the value
  // (HIGH or LOW) is not significant - it just needs to be an OUTPUT.
  // See: http://www.gammon.com.au/spi
  //
  pinMode(VS_SS, OUTPUT);

  // Now initialise the VS10xx
  digitalWrite(VS_XCS, HIGH);  //Deselect Control
  digitalWrite(VS_XDCS, HIGH); //Deselect Data
  digitalWrite(VS_RESET, LOW); //Put VS1053 into hardware reset

  // And then bring up SPI
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE0);

  //From page 12 of datasheet, max SCI reads are CLKI/7. Input clock is 12.288MHz. 
  //Internal clock multiplier is 1.0x after power up. 
  //Therefore, max SPI speed is 1.75MHz. We will use 1MHz to be safe.
  SPI.setClockDivider(SPI_CLOCK_DIV16); //Set SPI bus speed to 1MHz (16MHz / 16 = 1MHz)
  SPI.transfer(0xFF); //Throw a dummy byte at the bus

  delayMicroseconds(1);
  digitalWrite(VS_RESET, HIGH); //Bring up VS1053

  // Dummy read to ensure VS SPI bus in a known state
  VSReadRegister(SCI_MODE);

  // Perform software reset and initialise VS mode
  VSWriteRegister16(SCI_MODE, SM_SDINEW|SM_RESET);
  delay(200);


  // Enable real-time MIDI mode
  VSLoadUserCode();
}

void VSWriteRegister(unsigned char addressbyte, unsigned char highbyte, unsigned char lowbyte){
  while(!digitalRead(VS_DREQ)) ; //Wait for DREQ to go high indicating IC is available
  digitalWrite(VS_XCS, LOW); //Select control

  //SCI consists of instruction byte, address byte, and 16-bit data word.
  SPI.transfer(0); //Write instruction
  SPI.transfer(addressbyte);
  SPI.transfer(highbyte);
  SPI.transfer(lowbyte);
  while(!digitalRead(VS_DREQ)) ; //Wait for DREQ to go high indicating command is complete
  digitalWrite(VS_XCS, HIGH); //Deselect Control
}

// 16-bit interface to the above function.
//
void VSWriteRegister16 (unsigned char addressbyte, uint16_t value) {
  VSWriteRegister (addressbyte, value>>8, value&0xFF);
}

// Read a VS10xx register using the SCI (SPI command) bus.
//
uint16_t VSReadRegister(unsigned char addressbyte) {
  while(!digitalRead(VS_DREQ)) ; //Wait for DREQ to go high indicating IC is available
  digitalWrite(VS_XCS, LOW); //Select control
  
  SPI.transfer(0x03); //Read instruction
  SPI.transfer(addressbyte);
  delayMicroseconds(10);
  uint8_t d1 = SPI.transfer(0x00);
  uint8_t d2 = SPI.transfer(0x00);
  while(!digitalRead(VS_DREQ)) ; //Wait for DREQ to go high indicating command is complete
  digitalWrite(VS_XCS, HIGH); //Deselect control

  return ((d1<<8) | d2);
}

// Load a user plug-in over SPI.
//
// See the application and plug-in notes on the VLSI website for details.
//
void VSLoadUserCode(void) {
#ifdef TEST
  Serial.print("Loading User Code");
#endif
  for (int i=0; i<VS10xx_CODE_SIZE; i++) {
    uint8_t addr = pgm_read_byte_near(&vs10xx_atab[i]);
    uint16_t dat = pgm_read_word_near(&vs10xx_dtab[i]);
#ifdef TEST
    if (!(i%8)) Serial.print(".");
//    sprintf(teststr, "%4d --> 0x%04X => 0x%02x\n", i, dat, addr);
//    Serial.print(teststr);
#endif
    // Serial.print("teststr");
    VSWriteRegister16 (addr, dat);
  }

  // Set the start address of the application code (see rtmidi.pdf application note)
#ifdef VS1003_MODULE
  // Serial.print("teststr");
  VSWriteRegister16(SCI_AIADDR, 0x30);
#endif
}
